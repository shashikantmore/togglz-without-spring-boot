package com.moonspark;

import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;

public class WebAppInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletContext) {
        servletContext.setInitParameter("org.togglz.FEATURE_MANAGER_PROVIDED", "true");
    }
}
