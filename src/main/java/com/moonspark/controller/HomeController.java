package com.moonspark.controller;

import com.moonspark.MyFeatures;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping("/")
    public ResponseEntity<String> hello(){
        if ( MyFeatures.FEATURE_TWO.isActive() && MyFeatures.FEATURE_ONE.isActive()){
            return ResponseEntity.ok("Both features are active.");
        }
        if ( MyFeatures.FEATURE_ONE.isActive()) {
            return ResponseEntity.ok("First feature is active.");
        }
        if ( MyFeatures.FEATURE_TWO.isActive()) {
            return ResponseEntity.ok("Second feature is active.");
        }
        return ResponseEntity.ok("Dashboard without any features.");
    }
}
