package com.moonspark;

import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.manager.FeatureManagerBuilder;
import org.togglz.core.spi.FeatureManagerProvider;

@Component
public class SingletonFeatureManagerProvider implements FeatureManagerProvider {

    public FeatureManager getFeatureManager() {
        return new FeatureManagerBuilder()
                .togglzConfig(new AppTogglzConfig())
                .build();
    }

    public int priority() {
        return 30;
    }
}
