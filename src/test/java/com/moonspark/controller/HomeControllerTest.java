package com.moonspark.controller;

import com.moonspark.AppConfig;
import com.moonspark.MyFeatures;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.togglz.junit.TogglzRule;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class HomeControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Rule
    public TogglzRule togglzRule = TogglzRule.allEnabled(MyFeatures.class);

    private MockMvc mockMvc;

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void helloWithoutAnyFeatures() throws Exception {
        togglzRule.disable(MyFeatures.FEATURE_ONE);
        togglzRule.disable(MyFeatures.FEATURE_TWO);
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Dashboard without any features."));
    }

    @Test
    public void helloWithFirstFeatureActive() throws Exception {
        togglzRule.disable(MyFeatures.FEATURE_TWO);
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("First feature is active."));
    }

    @Test
    public void helloWithSecondFeatureActive() throws Exception {
        togglzRule.disable(MyFeatures.FEATURE_ONE);
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Second feature is active."));
    }

    @Test
    public void helloWithBothFeaturesActive() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Both features are active."));
    }

}